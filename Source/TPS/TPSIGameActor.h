// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "UObject/Interface.h"
#include "TPS/Weapons/TPSStateEffect.h"
//#include "TPS/FuncLibrary/Types.h"
#include "TPSIGameActor.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTPSIGameActor : public UInterface
{
	GENERATED_BODY()
};


class TPS_API ITPSIGameActor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	//UFUNCTION(BlueprintCallable, BlueprintImplenemtebleEvent, Category = "Event")
	//	void AviableForEffectBP();
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Event")
	//	bool AviableForEffect();

	//	virtual bool AviableForEffectOnlyCPP();

	virtual EPhysicalSurface GetSurfaceType();

	//Effect
	//UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		TArray<UTPSStateEffect*> Effects;

	virtual TArray<UTPSStateEffect*> GetAllCurrentEffects();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void RemoveEffect(UTPSStateEffect* RemovedEffect);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void AddEffect(UTPSStateEffect* NewEffect);

	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	//	void DropWeaponToWorld(FDropItem DropItemInfo);
	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	//	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};

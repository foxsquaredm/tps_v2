// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TPS/TPSIGameActor.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Weapons/TPSStateEffect.h"
#include "TPSEnvironmentStructure.generated.h"

UCLASS()
class TPS_API ATPSEnvironmentStructure : public AActor, public ITPSIGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATPSEnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfaceType() override;

	//Effect
	//UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<UTPSStateEffect*> Effects;

	TArray<UTPSStateEffect*> GetAllCurrentEffects() override;
	//void RemoveEffect(UTPSStateEffect* RemovedEffect) override;
	//void AddEffect(UTPSStateEffect* NewEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		//void RemoveEffect(UTPSStateEffect* RemovedEffect) override;
		void RemoveEffect(UTPSStateEffect* RemovedEffect);
		void RemoveEffect_Implementation(UTPSStateEffect* RemovedEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		//void AddEffect(UTPSStateEffect* NewEffect) override;
		void AddEffect(UTPSStateEffect* NewEffect);
		void AddEffect_Implementation(UTPSStateEffect* NewEffect) override;
	//End Interface

	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		UTPSStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		UTPSStateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> ParticleSystemEffects;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		FVector OffsetEffect = FVector(0);

	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);

	UFUNCTION()
		void SwitchEffect(UTPSStateEffect* Effect, bool bIsAdd);
};

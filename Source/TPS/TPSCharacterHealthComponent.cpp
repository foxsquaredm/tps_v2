// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSCharacterHealthComponent.h"
#include "Engine/World.h"

void UTPSCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	//Super::ReceiveDamage(ChangeValue);

	//Health += ChangeValue;
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f || ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);
		if (Shield < 0.0f)
		{
			//can show FX
			UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue_OnServer - Shield < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}

	//OnHealthChange.Broadcast(Health, ChangeValue);
	//
	//if (Health > 100.0f)
	//{
	//	Health = 100.0f;
	//}
	//else
	//{
	//	if (Health <= 0.0f)
	//	{
	//		OnDead.Broadcast();
	//		//DeadEvent();
	//	}
	//}
}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;
	//OnShieldChange.Broadcast(Shield, ChangeValue);
	ShieldChangeEvent_Multicast(Shield, ChangeValue);

	if (Shield > 100.f)
	{
		Shield = 100.f;
	}
	else
	{
		Shield = 0.0f;
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CoolDownShielTimer, this, &UTPSCharacterHealthComponent::CoolDawnShield, CoolDawnShieldRecoveryTime, false);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShielRecoveryRateTimer);
	}
}

void UTPSCharacterHealthComponent::CoolDawnShield()
{
	if (GetWorld())
	{
		//GetWorld()->TimerManadger->SetTimer(TimerHandle_ShielRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShielRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;

	if (tmp > 100.f)
	{
		Shield = 100.f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShielRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	//OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
	ShieldChangeEvent_Multicast(Shield, ShieldRecoveryValue);
}

float UTPSCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float newShield, float Damage)
{
	OnShieldChange.Broadcast(newShield, Damage);
}

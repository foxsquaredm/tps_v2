// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPSHealthComponent.h"
#include "TPSCharacterHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

UCLASS()
class TPS_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()

public:
	FTimerHandle TimerHandle_CoolDownShielTimer;
	FTimerHandle TimerHandle_ShielRecoveryRateTimer;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;

protected:
	float Shield = 100.0f;

public:

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
	//	float ShieldRecovery = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDawnShieldRecoveryTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryRate = 0.1f;

	//UFUNCTION(BlueprintCallable, Category = "Health")
		void ChangeHealthValue_OnServer(float ChangeValue) override;

		float GetCurrentShield();

		void ChangeShieldValue(float ChangeValue);

		void CoolDawnShield();

		void RecoveryShield();

	UFUNCTION(BlueprintCallable)
		float GetShieldValue();

	UFUNCTION(NetMulticast, Reliable)
		void ShieldChangeEvent_Multicast(float newShield, float Damage);
};

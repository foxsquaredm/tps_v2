// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Components/InputComponent.h"
#include "TPS/FuncLibrary/Types.h"
#include "TPS/Weapons/WeaponDefault.h"
#include "TPS/TPSIGameActor.h"
#include "TPS/Weapons/Projectiles/ProjectileDefault.h"
#include "TPS/Weapons/TPSStateEffect.h"
#include "TPSCharacter.generated.h"

UCLASS(Blueprintable)
class ATPSCharacter : public ACharacter, public ITPSIGameActor
{
	GENERATED_BODY()

protected:
	bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	virtual void BeginPlay() override;

public:
	ATPSCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	virtual void SetupPlayerInputComponent(UInputComponent* InputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	//FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	///** SpringArmComponent */
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	//class UCameraComponent* SpringArmComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		//class UActorComponent* InventoryComponent;
		class UTPSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UTPSCharacterHealthComponent* HealthComponent;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:

	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//	bool bIsAlive = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement ")
		TArray<UAnimMontage*> DeadAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UTPSStateEffect> AbilityEffect;

//private:

	//Weapon
	UPROPERTY(Replicated)
		AWeaponDefault* CurrentWeapon = nullptr;

	//for demo 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		FName InitWeaponName;

	UDecalComponent* CurrentCursor = nullptr;

	//Effect
	UPROPERTY(Replicated)
		TArray<UTPSStateEffect*> Effects;
	UPROPERTY(ReplicatedUsing = EffectAdd_OnRep)
		UTPSStateEffect* EffectAdd = nullptr;
	UPROPERTY(ReplicatedUsing = EffectRemove_OnRep)
		UTPSStateEffect* EffectRemove = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		TArray<UParticleSystemComponent*> ParticleSystemEffects;

	//Controll pawn ++
	UFUNCTION()
	void InputAxisX(float Value);

	UFUNCTION()
	void InputAxisY(float Value);

	//UFUNCTION()
	//void InputAltL_Press();
	//
	//UFUNCTION()
	//void InputAltL_Release();

	UFUNCTION()
		void InputShiftL_Press();

	UFUNCTION()
		void InputShiftL_Release();

	UFUNCTION()
		void InputAid_Press();

	UFUNCTION()
		void InputAid_Release();

	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();

	//Controll pawn --

	//UFUNCTION()
	//void InputWheelMouse(float Value);

	float CurrentDistance;
	float FinalDistance;
	bool ChangeZoom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool isAidChar = false;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	//bool isSprintChar = false;

	UFUNCTION()
		void InputZoomUp();

	UFUNCTION()
		void InputZoomDown();

	UFUNCTION()
		void CameraChangeZoom(int Value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bCanSprint = false;

	UFUNCTION()
		void MovementTick(float DeltaTime);

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	UFUNCTION()
		void CharacterUpdate();

	//UFUNCTION(BlueprintCallable)
	//	void ChangeMovementState(EMovementState NewMovementStat);

	//Func
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	//UFUNCTION(BlueprintCallable)
	//	void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState();

	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
		//void InitWeapon(FName IdWeaponName);
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();
	UFUNCTION(BlueprintCallable)
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintCallable)
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
		

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		TArray<UTPSStateEffect*> GetCurrentEffectsOnChar();


	UPROPERTY(VisibleAnywhere)
		UStaticMesh* EmptyMagazine;

	UFUNCTION(BlueprintCallable)
		void TrySwitchNextWeapon();
	UFUNCTION(BlueprintCallable)
		void TrySwitchPreviosWeapon();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int CurrentIndexWeapon = 0;

	//bool bIsSuccess = false;

	void TryAbilityEnabled();

	//Interface
	EPhysicalSurface GetSurfaceType() override;
	TArray<UTPSStateEffect*> GetAllCurrentEffects() override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		//void RemoveEffect(UTPSStateEffect* RemovedEffect) override;
		void RemoveEffect(UTPSStateEffect* RemovedEffect);
		void RemoveEffect_Implementation(UTPSStateEffect* RemovedEffect) override;
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
		//void AddEffect(UTPSStateEffect* NewEffect) override;
		void AddEffect(UTPSStateEffect* NewEffect);
		void AddEffect_Implementation(UTPSStateEffect* NewEffect) override;
	//End Interface

	UFUNCTION(BlueprintCallable)
	void CharDead();
	UFUNCTION(NetMulticast, Reliable)
	void EnableRagDoll_Multicast();

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetIsAlive();

	UFUNCTION(Server, Unreliable)
		void SetActorRotationByYaw_OnServer(float Yaw);
	UFUNCTION(NetMulticast, Unreliable)
		void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
		void SetMovementState_OnServer(EMovementState NewState);
	UFUNCTION(NetMulticast, Reliable)
		void SetMovementState_Multicast(EMovementState NewState);

	UFUNCTION(Server, Reliable)
		void TryReloadWeapon_OnServer();

	UFUNCTION(NetMulticast, Reliable)
		void PlayAnim_Multicast(UAnimMontage* Anim);

	UFUNCTION()
		void EffectAdd_OnRep();
	UFUNCTION()
		void EffectRemove_OnRep();

	UFUNCTION(Server, Reliable)
		void ExecuteEffectAdded_OnServer(UParticleSystem* ExecuteFX);
	UFUNCTION(NetMulticast, Reliable)
		void ExecuteEffectAdded_Multicast(UParticleSystem* ExecuteFX);


	UFUNCTION()
		void SwitchEffect(UTPSStateEffect* Effect, bool bIsAdd);

};
// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TPS/Character/TPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Math/UnrealMathUtility.h"
#include "Math/Vector.h"
#include "TPS/Game/TPSGameInstance.h"

#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "TPS/TPSInventoryComponent.h"
#include "TPS/TPSCharacterHealthComponent.h"

#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"

#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

#include "Engine/World.h"
#include "TPS/TPS.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Engine/ActorChannel.h"


ATPSCharacter::ATPSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	//InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("UTPSInventoryComponent"));
	InventoryComponent = CreateDefaultSubobject<UTPSInventoryComponent>(TEXT("InventoryComponent"));
	HealthComponent = CreateDefaultSubobject<UTPSCharacterHealthComponent>(TEXT("HealthComponent"));

	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATPSCharacter::InitWeapon);
	}

	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &ATPSCharacter::CharDead);
	}

	//// Create a Spring arm...
	//SpringArmComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SpringArm"));
	//SpringArmComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	//SpringArmComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	//// Create a decal in the world to show the cursor's location
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/BluePrints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	//if (DecalMaterialAsset.Succeeded())
	//{
	//	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	//}
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	CurrentDistance = CameraBoom->TargetArmLength;
	FinalDistance = CurrentDistance;

	//Network
	bReplicates = true;
}

void ATPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	//AWeaponDefault* myWeapon = GetCurrentWeapon();
	//InitWeapon(InitWeaponName, myWeapon->WeaponInfo, 0);

	if (GetWorld() && GetWorld()->GetNetMode() != NM_DedicatedServer)
	{
		if (CursorMaterial && GetLocalRole() == ROLE_AutonomousProxy || GetLocalRole() == ROLE_Authority)
		{
			CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		}
	}
}

void ATPSCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//if (CursorToWorld != nullptr)
	//{
	//	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	//	{
	//		if (UWorld* World = GetWorld())
	//		{
	//			FHitResult HitResult;
	//			FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
	//			FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
	//			FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
	//			Params.AddIgnoredActor(this);
	//			World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
	//			FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
	//			CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
	//		}
	//	}
	//	else if (APlayerController* PC = Cast<APlayerController>(GetController()))
	//	{
	//		FHitResult TraceHitResult;
	//		PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
	//		FVector CursorFV = TraceHitResult.ImpactNormal;
	//		FRotator CursorR = CursorFV.Rotation();
	//		CursorToWorld->SetWorldLocation(TraceHitResult.Location);
	//		CursorToWorld->SetWorldRotation(CursorR);
	//	}
	//}

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC && myPC->IsLocalPlayerController())
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);

	/////////////////////////////////////////////////////////////////////
	if (ChangeZoom)
	{
		CameraBoom->TargetArmLength = FMath::FInterpTo(CurrentDistance, FinalDistance, DeltaSeconds, 100.0f);
		ChangeZoom = false;
	}


	//	//test "can sprint"
	//	bCanSprint = GetActorForwardVector().Equals(GetVelocity().GetSafeNormal(), 0.1f);
	//	if (isSprintChar && bCanSprint)
	//	{
	//		ChangeMovementState(EMovementState::Run_State);
	//	}
	//	else
	//	{
	//		ChangeMovementState(EMovementState::Walk_State);
	//	}

}

void ATPSCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATPSCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATPSCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("CameraZoomUp"), IE_Pressed, this, &ATPSCharacter::InputZoomUp);
	NewInputComponent->BindAction(TEXT("CameraZoomDown"), IE_Pressed, this, &ATPSCharacter::InputZoomDown);

	//NewInputComponent->BindAction(TEXT("TurnSpeed"), IE_Pressed, this, &ATPSCharacter::InputAltL_Press); 
	//NewInputComponent->BindAction(TEXT("TurnSpeed"), IE_Released, this, &ATPSCharacter::InputAltL_Release);

	NewInputComponent->BindAction(TEXT("Sprint"), IE_Pressed, this, &ATPSCharacter::InputShiftL_Press);
	NewInputComponent->BindAction(TEXT("Sprint"), IE_Released, this, &ATPSCharacter::InputShiftL_Release);

	NewInputComponent->BindAction(TEXT("Aid"), IE_Pressed, this, &ATPSCharacter::InputAid_Press);
	NewInputComponent->BindAction(TEXT("Aid"), IE_Released, this, &ATPSCharacter::InputAid_Release);

	NewInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &ATPSCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &ATPSCharacter::InputAttackReleased);

	NewInputComponent->BindAction(TEXT("Reload"), EInputEvent::IE_Pressed, this, &ATPSCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNexWeapon"), EInputEvent::IE_Pressed, this, &ATPSCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATPSCharacter::TrySwitchPreviosWeapon);
}

void ATPSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATPSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATPSCharacter::MovementTick(float DeltaTime)
{
	if (HealthComponent && HealthComponent->GetIsAlive())
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
			AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

			FString SENum = UEnum::GetValueAsString(GetMovementState());
			UE_LOG(LogTPS_Net, Warning, TEXT("Movement state - %s"), *SENum);

			//APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			//if (myController)
			//{
			//	FHitResult HitResult;
			//	myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, HitResult);

			//	float FindRotatorResult = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
			//	SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResult, 0.0f)));
			//}

			if (MovementState == EMovementState::SprintRun_State)
			{
				FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
				FRotator myRotator = myRotationVector.ToOrientationRotator();
				SetActorRotation((FQuat(myRotator)));
				SetActorRotationByYaw_OnServer(myRotator.Yaw);
			}
			else
			{
				APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
				if (myController)
				{
					FHitResult ResultHit;
					//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);// bug was here Config\DefaultEngine.Ini
					myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

					float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
					SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
					SetActorRotationByYaw_OnServer(FindRotaterResultYaw);

					if (CurrentWeapon)
					{
						FVector Displacement = FVector(0);
						bool bIsReduceDispersion = false;
						switch (MovementState)
						{
						case EMovementState::Aim_State:
							Displacement = FVector(0.0f, 0.0f, 160.0f);
							//CurrentWeapon->ShouldReduceDispersion = true;
							bIsReduceDispersion = true;
							break;
							//case EMovementState::AimWalk_State:
							//	CurrentWeapon->ShouldReduceDispersion = true;
							//	Displacement = FVector(0.0f, 0.0f, 160.0f);
							//	break;
							//case EMovementState::Walk_State:
							//	Displacement = FVector(0.0f, 0.0f, 120.0f);
							//	CurrentWeapon->ShouldReduceDispersion = false;
							//	break;
						case EMovementState::Run_State:
							Displacement = FVector(0.0f, 0.0f, 120.0f);
							//CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EMovementState::SprintRun_State:
							break;
						default:
							break;
						}

						//CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
						CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(ResultHit.Location + Displacement, bIsReduceDispersion);
						//aim cursor like 3d Widget?
					}
				}
			}
		}
	}
}

void ATPSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
		//case EMovementState::AimWalk_State:
		//	ResSpeed = MovementSpeedInfo.AimSpeedWalk;
		//	break;

	//case EMovementState::Walk_State:
	//	ResSpeed = MovementInfo.WalkSpeed;
	//	break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;

	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;

}

//void ATPSCharacter::ChangeMovementState(EMovementState NewMovementStat)
void ATPSCharacter::ChangeMovementState()
{
	//MovementState = NewMovementStat;
	EMovementState NewState = EMovementState::Run_State;

	if (!SprintRunEnabled && !AimEnabled)
	{
		//MovementState = EMovementState::Run_State;
		NewState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			AimEnabled = false;
			//MovementState = EMovementState::SprintRun_State;
			NewState = EMovementState::SprintRun_State;
		}

		if (AimEnabled)
		{
			//MovementState = EMovementState::Aim_State;
			NewState = EMovementState::Aim_State;
		}
	}

	SetMovementState_OnServer(NewState);

	//CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//myWeapon->UpdateStateWeapon(MovementState);
		myWeapon->UpdateStateWeapon_OnServer(NewState);
	}

}

EMovementState ATPSCharacter::GetMovementState()
{
	return MovementState;
}

AWeaponDefault* ATPSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATPSCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
//void ATPSCharacter::InitWeapon(FName IdWeaponName)
{
	//on server
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	//if (GetNetMode() == ENetMode::NM_ListenServer)
	//{
	//	UE
	//}

	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;
					myWeapon->CurrentWeaponIDName = IdWeaponName;

					myWeapon->WeaponSetting = myWeaponInfo;
					//myWeapon->WeaponInfo.Round = myWeaponInfo.MaxRound;
					//Remove !!! Debug
					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon_OnServer(MovementState);

					myWeapon->WeaponInfo = WeaponAdditionalInfo;
					//if (InventoryComponent)
					//{
					//	CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(IdWeaponName);
					//}
					CurrentIndexWeapon = NewCurrentIndexWeapon;

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPSCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPSCharacter::WeaponReloadEnd);

					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATPSCharacter::WeaponFireStart);

					//after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
					{
						CurrentWeapon->InitReload();
						if (InventoryComponent)
						{
							InventoryComponent->OnWeaponAmmoEmpty.Broadcast(myWeapon->WeaponSetting.WeaponType);
						}
					}
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}

}

void ATPSCharacter::TryReloadWeapon()
{
	if (HealthComponent && HealthComponent->GetIsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		TryReloadWeapon_OnServer();
	}

}

void ATPSCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	FVector SpawnLocation = FVector(0);
	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), CurrentWeapon->WeaponSetting.SoundReloadWeapon, SpawnLocation);

	//drop magazine


	WeaponReloadStart_BP(Anim);
}

void ATPSCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}

	WeaponReloadEnd_BP(bIsSuccess);

}

void ATPSCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->WeaponInfo);
	}

	WeaponFireStart_BP(Anim);
}

UDecalComponent* ATPSCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

TArray<UTPSStateEffect*> ATPSCharacter::GetCurrentEffectsOnChar()
{
	return Effects;
}

void ATPSCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

void ATPSCharacter::TrySwitchPreviosWeapon()
{
	if (CurrentWeapon && CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

void ATPSCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)//TODO Cool down
	{
		UTPSStateEffect* NewEffect = NewObject<UTPSStateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this, NAME_None);
		}
	}
}

EPhysicalSurface ATPSCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	//UStaticMeshComponent* MyMesh = Cast<UStaticMeshComponent>(UStaticMeshComponent::StaticClass());

	if (HealthComponent)
	{
		if (HealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* MyMaterial = GetMesh()->GetMaterial(0);
				if (MyMaterial)
				{
					Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}


	return Result;
}

TArray<UTPSStateEffect*> ATPSCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATPSCharacter::RemoveEffect_Implementation(UTPSStateEffect* RemovedEffect)
{
	Effects.Remove(RemovedEffect);

	if (!RemovedEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemovedEffect, false);
		EffectRemove = RemovedEffect;
	}
}

void ATPSCharacter::AddEffect_Implementation(UTPSStateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}
}

void ATPSCharacter::CharDead()
{
	if (HasAuthority())
	{
		float TimeDeadAnim = 0.0f;
		int32 rnd = FMath::RandHelper(DeadAnim.Num());

		if (DeadAnim.IsValidIndex(rnd) && DeadAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			//GetMesh()->GetAnimInstance()->Montage_Play(DeadAnim[rnd]);
			PlayAnim_Multicast(DeadAnim[rnd]);
		}

		TimeDeadAnim = DeadAnim[rnd]->GetPlayLength();
		//bIsAlive = false;

		if (GetController())
		{
			GetController()->UnPossess();
		}
		//UnPossessed();

		//Timer ragdoll
		GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATPSCharacter::EnableRagDoll_Multicast, TimeDeadAnim, false);

	}
	else
	{
		if (GetCursorToWorld())
		{
			GetCursorToWorld()->SetVisibility(false);
		}
		AttackCharEvent(false);
	}

	CharDead_BP();
}

void ATPSCharacter::EnableRagDoll_Multicast_Implementation()
{

	//UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::EnableRagDoll"));

	if (GetMesh())
	{
		if (GetCapsuleComponent())
		{
			GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
		}
		GetMesh()->SetCollisionObjectType(ECC_PhysicsBody);
		GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Block);
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}

}

float ATPSCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	if (HealthComponent && HealthComponent->GetIsAlive())
	{
		HealthComponent->ChangeHealthValue_OnServer(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
			if (myProjectile)
			{
				UTypes::AddEffectBySurfaceType(this, NAME_None, myProjectile->ProjectileSetting.Effect, GetSurfaceType());	//to do Name_None - bone for radial damage
				
			}
	}

	return ActualDamage;
}

bool ATPSCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

bool ATPSCharacter::GetIsAlive()
{
	bool result = false;
	if (HealthComponent)
	{
		result = HealthComponent->GetIsAlive();
	}

	return result;
}

void ATPSCharacter::PlayAnim_Multicast_Implementation(UAnimMontage* Anim)
{
	if (GetMesh()->GetAnimInstance())
	{
		GetMesh()->GetAnimInstance()->Montage_Play(Anim);
	}
}

void ATPSCharacter::TryReloadWeapon_OnServer_Implementation()
{

	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
		CurrentWeapon->InitReload();

}

void ATPSCharacter::SetActorRotationByYaw_OnServer_Implementation(float Yaw)
{
	SetActorRotationByYaw_Multicast(Yaw);
}

void ATPSCharacter::SetActorRotationByYaw_Multicast_Implementation(float Yaw)
{
	if (Controller && !Controller->IsLocalPlayerController())
	{
		SetActorRotation(FQuat(FRotator(0.0f, Yaw, 0.0f)));
	}
}

void ATPSCharacter::SetMovementState_OnServer_Implementation(EMovementState NewState)
{
	SetMovementState_Multicast(NewState);
}

void ATPSCharacter::SetMovementState_Multicast_Implementation(EMovementState NewState)
{
	MovementState = NewState;
	CharacterUpdate();
}

void ATPSCharacter::CharDead_BP_Implementation()
{
	//BP

}

void ATPSCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATPSCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void ATPSCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATPSCharacter::InputZoomUp()
{
	CameraChangeZoom(1);
}

void ATPSCharacter::InputZoomDown()
{
	CameraChangeZoom(-1);
}

void ATPSCharacter::CameraChangeZoom(int Value)
{
	float SizeZoom = 100;
	ChangeZoom = true;

	CurrentDistance = CameraBoom->TargetArmLength;

	if (Value == 1)
	{
		if (CurrentDistance < 1600.0f)
		{
			FinalDistance = CurrentDistance + SizeZoom;
		}

	}
	else if (Value == -1)
	{
		if (CurrentDistance > 600.0f)
		{
			FinalDistance = CurrentDistance - SizeZoom;
		}
	}
}



//void ATPSCharacter::InputAltL_Press()
//{
//	ChangeMovementState(EMovementState::Walk_State);
//}
//
//void ATPSCharacter::InputAltL_Release()
//{
//	ChangeMovementState(EMovementState::Run_State);
//}

void ATPSCharacter::InputShiftL_Press()
{
	//isSprintChar = true;
	//if (bCanSprint)
	//{
	//	ChangeMovementState(EMovementState::Run_State);
	//}
	//else
	//{
	//	ChangeMovementState(EMovementState::Walk_State);
	//}

	SprintRunEnabled = true;
	ChangeMovementState();
}

void ATPSCharacter::InputShiftL_Release()
{
	//isSprintChar = false;
	//ChangeMovementState(EMovementState::Walk_State);

	SprintRunEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::InputAid_Press()
{
	isAidChar = true;
	//ChangeMovementState(EMovementState::Aim_State);

	AimEnabled = true;
	ChangeMovementState();
}

void ATPSCharacter::InputAid_Release()
{
	isAidChar = false;

	////it is necessary to return the state or check the pressed buttons
	////dut while just walking
	//ChangeMovementState(EMovementState::Walk_State);

	AimEnabled = false;
	ChangeMovementState();
}

void ATPSCharacter::InputAttackPressed()
{
	if (HealthComponent && HealthComponent->GetIsAlive())
	{
		AttackCharEvent(true);
	}
}

void ATPSCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATPSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}
}

void ATPSCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPSCharacter, MovementState);
	DOREPLIFETIME(ATPSCharacter, CurrentWeapon);
	//DOREPLIFETIME(ATPSCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(ATPSCharacter, Effects);
	DOREPLIFETIME(ATPSCharacter, EffectAdd);
	DOREPLIFETIME(ATPSCharacter, EffectRemove);
}

void ATPSCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATPSCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATPSCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATPSCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("Spine_01"));
}

void ATPSCharacter::SwitchEffect(UTPSStateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = Effect->NameBone;
			FVector Loc = FVector(0);

			USkeletalMeshComponent* myMesh = GetMesh();
			if (myMesh)
			{
				UParticleSystemComponent* newParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(newParticleSystem);
			}
		}
	}
	else
	{
		//if (Effect && Effect->ParticleEffect && !Effect->bIsAutoDestroyParticleEffect)
		if (Effect && Effect->ParticleEffect)
		{
			int32 i = 0;
			bool bIsFind = false;
			if (ParticleSystemEffects.Num() > 0)
			{
				while (i < ParticleSystemEffects.Num() && !bIsFind)
				{
					if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsFind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
					i++;
				}
			}
		}
	}
}

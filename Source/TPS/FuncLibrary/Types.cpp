// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "TPS/TPSIGameActor.h"
#include "TPS/TPSIGameActor.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, FName NameBoneHit, TSubclassOf<UTPSStateEffect> AddEffecClass, EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffecClass)
	{
			UTPSStateEffect* myEffect = Cast<UTPSStateEffect>(AddEffecClass->GetDefaultObject());
			if (myEffect)
			{
				bool bIsHavePossibleSurface = false;
				int8 i = 0;
				while (i < myEffect->PossibleIteractSurface.Num() && !bIsHavePossibleSurface)
				{
					if (myEffect->PossibleIteractSurface[i] == SurfaceType)
					{
						bIsHavePossibleSurface = true;
						bool bIsCanAddEffect = false;
						if (!myEffect->bIsStakable)
						{
							int8 j = 0;
							TArray<UTPSStateEffect*> CurrentEffects;
							ITPSIGameActor* myInterface = Cast<ITPSIGameActor>(TakeEffectActor);
							if (myInterface)
							{
								CurrentEffects = myInterface->GetAllCurrentEffects();
							}
							if (CurrentEffects.Num() > 0)
							{
								while (j < CurrentEffects.Num() && !bIsCanAddEffect)
								{
									if (CurrentEffects[j]->GetClass() != AddEffecClass)
									{
										bIsCanAddEffect = true;
									}
									j++;
								}
							}
							else
							{
								bIsCanAddEffect = true;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}

						if (bIsCanAddEffect)
						{
							
							//UTPSStateEffect* NewEffect = NewObject<UTPSStateEffect>(Hit.GetActor(), FName("Effect"));
							UTPSStateEffect* NewEffect = NewObject<UTPSStateEffect>(TakeEffectActor, AddEffecClass);
							if (NewEffect)
							{
								NewEffect->InitObject(TakeEffectActor, NameBoneHit);
							}
						}
					}
					i++;
				}

			}
	}
}

void UTypes::ExecuteEffectAdded(UParticleSystem* ExecuteFX, AActor* target, FVector offset, FName Socket)
{
	FName SocketToAttach = Socket;
	FVector Loc = offset;

	if (target)
	{

		ACharacter* myCharacter = Cast<ACharacter>(target);

		if (myCharacter && myCharacter->GetMesh())
		{
			UGameplayStatics::SpawnEmitterAttached(ExecuteFX, myCharacter->GetMesh(), SocketToAttach, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}
	else
	{
		if (target->GetRootComponent())
		{
			UGameplayStatics::SpawnEmitterAttached(ExecuteFX, target->GetRootComponent(), SocketToAttach, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}
}

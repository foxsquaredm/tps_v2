// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSInventoryComponent.h"
#include "Game/TPSGameInstance.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UTPSInventoryComponent::UTPSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	
	SetIsReplicatedByDefault(true);
}


// Called when the game starts
void UTPSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	

}


// Called every frame
void UTPSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//bool UTPSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo)
bool UTPSInventoryComponent::SwitchWeaponToIndex(int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num()-1)
	{
		CorrectIndex = 0;
	}
	else
	{
		if (ChangeToIndex < 0)
		{
			CorrectIndex = WeaponSlots.Num() - 1;
		}
	}

	FName NewIDWeapon;
	FAdditionalWeaponInfo NewAdditionalWeapon;
	int32 NewCurrentIndex = 0;

	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (!(WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0))
			{
				//good begin. have ammo
				bIsSuccess = true;
			}
			else
			{
				UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

					//int8 i = 0;
					//while (i < WeaponSlots.Num() && !bIsSuccess)
					//{
					//	if (i == CorrectIndex)
					//	{
					//		if (WeaponSlots[i].NameItem.IsNone())
					//		{
					//			NewIDWeapon = WeaponSlots[i].NameItem;
					//			NewAdditionalWeapon = WeaponSlots[i].AdditionalInfo;
					//			bIsSuccess = true;
					//		}
					//	}
					//	i++;
					//}

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlot.Num() && !bIsFind)
					{
						if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Cout > 0)
						{
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}

				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIDWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalWeapon = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}


	if (!bIsSuccess)
	{
		if (bIsForward)
		{
			int8 iteration = 0;
			int8 Seconditeration = 0;
			while (iteration < WeaponSlots.Num() && !bIsSuccess)
			{
				iteration++;
				int8 tmpIndex = ChangeToIndex + iteration;
				if (WeaponSlots.IsValidIndex(tmpIndex))
				{
					if (!WeaponSlots[tmpIndex].NameItem.IsNone())
					{
						if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
						{
							bIsSuccess = true;
							NewIDWeapon = WeaponSlots[tmpIndex].NameItem;
							NewAdditionalWeapon = WeaponSlots[tmpIndex].AdditionalInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
							if (myGI)
							{
								FWeaponInfo myInfo;
								myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlot.Num() && !bIsFind)
								{
									if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Cout > 0)
									{
										bIsSuccess = true;
										bIsFind = true;
										NewIDWeapon = WeaponSlots[tmpIndex].NameItem;
										NewAdditionalWeapon = WeaponSlots[tmpIndex].AdditionalInfo;
										NewCurrentIndex = tmpIndex;
									}
									j++;
								}

							}
						}
					}
				}
				else
				{
					if (OldIndex != Seconditeration)
					{
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									bIsSuccess = true;
									NewIDWeapon = WeaponSlots[Seconditeration].NameItem;
									NewAdditionalWeapon = WeaponSlots[Seconditeration].AdditionalInfo;
									NewCurrentIndex = Seconditeration;
								}
								else
								{
									UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
									if (myGI)
									{
										FWeaponInfo myInfo;
										myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

										bool bIsFind = false;
										int8 j = 0;
										while (j < AmmoSlot.Num() && !bIsFind)
										{
											if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Cout > 0)
											{
												bIsSuccess = true;
												bIsFind = true;
												NewIDWeapon = WeaponSlots[Seconditeration].NameItem;
												NewAdditionalWeapon = WeaponSlots[Seconditeration].AdditionalInfo;
												NewCurrentIndex = Seconditeration;
											}
											j++;
										}

									}
								}
							}
						}
					}
					else
					{
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									//do nothing
								}
								else
								{
									UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
									if (myGI)
									{
										FWeaponInfo myInfo;
										myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

										bool bIsFind = false;
										int8 j = 0;
										while (j < AmmoSlot.Num() && !bIsFind)
										{
											if (AmmoSlot[j].WeaponType == myInfo.WeaponType) 
											{
												if (AmmoSlot[j].Cout > 0)
												{
													//do nothing
												}
												else
												{
													UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - init pistol"));
												}
											}
											j++;
										}

									}
								}
							}
						}
					}
					Seconditeration++;
				}
			}
		}
		else
		{
		int8 iteration = 0;
		int8 Seconditeration = WeaponSlots.Num() - 1;
		while (iteration < WeaponSlots.Num() && !bIsSuccess)
		{
			iteration++;
			int8 tmpIndex = ChangeToIndex - iteration;
			if (WeaponSlots.IsValidIndex(tmpIndex))
			{
				if (!WeaponSlots[tmpIndex].NameItem.IsNone())
				{
					if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
					{
						bIsSuccess = true;
						NewIDWeapon = WeaponSlots[tmpIndex].NameItem;
						NewAdditionalWeapon = WeaponSlots[tmpIndex].AdditionalInfo;
						NewCurrentIndex = tmpIndex;
					}
					else
					{
						UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
						if (myGI)
						{
							FWeaponInfo myInfo;
							myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlot.Num() && !bIsFind)
							{
								if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Cout > 0)
								{
									bIsSuccess = true;
									bIsFind = true;
									NewIDWeapon = WeaponSlots[tmpIndex].NameItem;
									NewAdditionalWeapon = WeaponSlots[tmpIndex].AdditionalInfo;
									NewCurrentIndex = tmpIndex;
								}
								j++;
							}

						}
					}
				}
			}
			else
			{
				if (OldIndex != Seconditeration)
				{
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
							{
								bIsSuccess = true;
								NewIDWeapon = WeaponSlots[Seconditeration].NameItem;
								NewAdditionalWeapon = WeaponSlots[Seconditeration].AdditionalInfo;
								NewCurrentIndex = Seconditeration;
							}
							else
							{
								UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
								if (myGI)
								{
									FWeaponInfo myInfo;
									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlot.Num() && !bIsFind)
									{
										if (AmmoSlot[j].WeaponType == myInfo.WeaponType && AmmoSlot[j].Cout > 0)
										{
											bIsSuccess = true;
											bIsFind = true;
											NewIDWeapon = WeaponSlots[Seconditeration].NameItem;
											NewAdditionalWeapon = WeaponSlots[Seconditeration].AdditionalInfo;
											NewCurrentIndex = Seconditeration;
										}
										j++;
									}

								}
							}
						}
					}
				}
				else
				{
					if (WeaponSlots.IsValidIndex(Seconditeration))
					{
						if (!WeaponSlots[Seconditeration].NameItem.IsNone())
						{
							if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
							{
								//do nothing
							}
							else
							{
								UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
								if (myGI)
								{
									FWeaponInfo myInfo;
									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlot.Num() && !bIsFind)
									{
										if (AmmoSlot[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlot[j].Cout > 0)
											{
												//do nothing
											}
											else
											{
												UE_LOG(LogTemp, Error, TEXT("UTPSInventoryComponent::SwitchWeaponToIndex - init pistol"));
											}
										}
										j++;
									}

								}
							}
						}
					}
				}
				Seconditeration--;
			}
		}
		}

	}

	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIDWeapon, NewAdditionalWeapon, NewCurrentIndex);
	}

	return bIsSuccess;
}

FAdditionalWeaponInfo UTPSInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			//if (WeaponSlots[i].IndexSlot == IndexWeapon)
			if (i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No found weapon with index - %d"), IndexWeapon);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Found weapon with index - %d"), IndexWeapon);
		}
	}

	return result;
}

int32 UTPSInventoryComponent::GetWeaponIndexSlotByName(FName IDWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;

	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IDWeaponName)
		{
			result = i;
			bIsFind = true;
		}
		i++;
	}

	return result;
}

FName UTPSInventoryComponent::GetWeaponNameBySlotIndex(int32 IndexSlot)
{
	FName result;
	if (WeaponSlots.IsValidIndex(IndexSlot))
	{
		result = WeaponSlots[IndexSlot].NameItem;
	}

	return result;
}

void UTPSInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		int32 i = 0;
		bool bIsFind = false;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;
				////to AmmoCout change
				//OnAmmoChange.Broadcast(EWeaponType::ShotGunType, 32);
				
				//OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
				OnWeaponAICEvent_Multicast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - No found weapon with index - %d"), IndexWeapon);
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTPSInventoryComponent::SetAdditionalInfoWeapon - Found weapon with index - %d"), IndexWeapon);
	}
}

void UTPSInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlot.Num() && !bIsFind)
	{
		if (AmmoSlot[i].WeaponType == TypeWeapon)
		{
			AmmoSlot[i].Cout += CoutChangeAmmo;
			//AmmoSlot[i].Cout -= CoutChangeAmmo;
			if (AmmoSlot[i].Cout > AmmoSlot[i].MaxCout)
			{
				AmmoSlot[i].Cout = AmmoSlot[i].MaxCout;
			}
			AmmoChangeEvent_Multicast(AmmoSlot[i].WeaponType, AmmoSlot[i].Cout);
			//OnAmmoChange.Broadcast(AmmoSlot[i].WeaponType, AmmoSlot[i].Cout);
			
			bIsFind = true;
		}
		i++;
	}

}

bool UTPSInventoryComponent::CheckAmmoForWeapon(EWeaponType TypeWeapon, int8 &AviableAmmoForWeapon)
{
	AviableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlot.Num() && !bIsFind)
	{
		if (AmmoSlot[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AviableAmmoForWeapon = AmmoSlot[i].Cout;
			if (AmmoSlot[i].Cout > 0)
			{
				return true;
			}
		}
		i++;
	}

	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);

	return false;
}

bool UTPSInventoryComponent::CheckCanTakenAmmo(EWeaponType AmmoType)
{
	bool result = false;
	int8 i = 0;
	while (i < AmmoSlot.Num() && !result)
	{
		if (AmmoSlot[i].WeaponType == AmmoType && AmmoSlot[i].Cout < AmmoSlot[i].MaxCout)
		{
			result = true;
		}

		i++;
	}

	return result;
}

bool UTPSInventoryComponent::CheckCanTakenIWeapon(int32 &FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}


	return false;
}

bool UTPSInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, int32 IndexSlot, int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo)
{
	bool result = false;
	
	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		WeaponSlots[IndexSlot] = NewWeapon;
		SwitchWeaponToIndex(CurrentIndexWeaponChar, -1, WeaponSlots[IndexSlot].AdditionalInfo, true);

		OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		result = true;
	}

	return result;
}

bool UTPSInventoryComponent::GetDropItemInfoFromInventory(int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool result = false;
	
	FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		result = myGI->GetDropItemInfoByName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}

	return result;
}

void UTPSInventoryComponent::OnWeaponAICEvent_Multicast_Implementation(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo);
}

//bool UTPSInventoryComponent::TryGetWeaponToInventory_OnServer(FWeaponSlot NewWeapon)
void UTPSInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(AActor* PickupActor, FWeaponSlot NewWeapon)
{
	int32 indexSlot = -1;
	if (CheckCanTakenIWeapon(indexSlot))
	{
		if (WeaponSlots.IsValidIndex(indexSlot))
		{
			WeaponSlots[indexSlot] = NewWeapon;
			//FWeaponSlotTest NewSlots;
			//NewSlots.Slot = WeaponSlots;
			//OnUpdateWeaponSlots.Broadcast(WeaponSlots);
			OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon);
			
			//return true;
			if (PickupActor)
			{
				PickupActor->Destroy();
			}
		}
	}

	//return false;
}

void UTPSInventoryComponent::AmmoChangeEvent_Multicast_Implementation(EWeaponType TypeWeapon, int32 Cout)
{
	OnAmmoChange.Broadcast(TypeWeapon, Cout);
}

void UTPSInventoryComponent::InitInventory_OnServer_Implementation(const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlot = NewAmmoSlotsInfo;

	for (int8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UTPSGameInstance* myGI = Cast<UTPSGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			if (WeaponSlots[i].NameItem.IsNone())
			{
				//FWeaponInfo Info;
				//if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
				//{
				//	WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
				//}
				////else
				////{
				////	WeaponSlots.RemoveAt(i);
				////	i--;
				////}
			}
		}
	}

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
		}
	}

}

void UTPSInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPSInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UTPSInventoryComponent, AmmoSlot);

}
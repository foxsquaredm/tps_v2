// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TPSStateEffect.generated.h"


//UCLASS(BlueprintAssignable, BlueprintType)
UCLASS()
class TPS_API UTPSStateEffect : public UObject
{
	GENERATED_BODY()
	
public:

	bool IsSupportedForNetworking() const override {return true;};
	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	//virtual void ExecuteEffect(float DEltaTime);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleIteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStakable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		UParticleSystem* ParticleEffect = nullptr;
		//UParticleSystemComponent* ParticleEmitter = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		bool bIsAutoDestroyParticleEffect = false;

	AActor* myActor = nullptr;
	UPROPERTY(Replicated)
		FName NameBone;
};

UCLASS()
class TPS_API UTPS_StateEffect_ExecuteOnce : public UTPSStateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute once")
		float Power = 20.0f;
};

UCLASS()
class TPS_API UTPS_StateEffect_ExecuteTimer : public UTPSStateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute once")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute once")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute once")
		float RateTimer = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting execute once")
		//UParticleEmitter* ParticleEffect = nullptr;
		//UParticleSystem* ParticleEffect = nullptr;

		//UParticleSystemComponent* ParticleEmitter = nullptr;
};
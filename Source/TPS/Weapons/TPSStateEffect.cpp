// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSStateEffect.h"
#include "TPS/TPSCharacterHealthComponent.h"
//#include "Particles/ParticleSystemComponent.h"
//#include "Engine/SkeletalMesh.h"
#include "TPS/TPSIGameActor.h"
#include "Net/UnrealNetwork.h"

bool UTPSStateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;
	NameBone = NameBoneHit;

	ITPSIGameActor* myIntaerface = Cast<ITPSIGameActor>(myActor);
	if (myIntaerface)
	{
		myIntaerface->Execute_AddEffect(myActor, this);

	}

	return true;
}

//void UTPSStateEffect::ExecuteEffect(float DEltaTime)
//{
//}

void UTPSStateEffect::DestroyObject()
{
	ITPSIGameActor* myIntaerface = Cast<ITPSIGameActor>(myActor);
	if (myIntaerface)
	{
		myIntaerface->Execute_RemoveEffect(myActor, this);

	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
		
}

bool UTPS_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UTPS_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);

		}
	}
	
	DestroyObject();
}

bool UTPS_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTPS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTPS_StateEffect_ExecuteTimer::Execute, Timer, true);
	}

	//if (ParticleEffect)
	//{
	//	FName NameBoneToAttached = NameBoneHit;
	//	FVector Loc;

	//	USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	//	if (myMesh)
	//	{
	//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	//	}
	//	else
	//	{
	//		ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
	//	}
	//}
	
	return true;
}

void UTPS_StateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	//ParticleEmitter->DestroyComponent();
	//ParticleEmitter = nullptr;

	Super::DestroyObject();
}

void UTPS_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UTPSHealthComponent* myHealthComp = Cast<UTPSHealthComponent>(myActor->GetComponentByClass(UTPSHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);

		}
	}
}

void UTPSStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPSStateEffect, NameBone);
}
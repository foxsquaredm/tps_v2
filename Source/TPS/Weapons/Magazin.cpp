// Fill out your copyright notice in the Description page of Project Settings.


#include "Magazin.h"
#include "Engine/StaticMesh.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
//AMagazin::AMagazin(UStaticMesh* MagazineDrop)
AMagazin::AMagazin()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshMagazin = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshMagazin->SetGenerateOverlapEvents(false);
	StaticMeshMagazin->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
	StaticMeshMagazin->SetupAttachment(RootComponent);
	//StaticMeshMagazin = WeaponSetting.MagazineDrop;
	//StaticMeshMagazin->SetStaticMesh(MagazineDrop);
	StaticMeshMagazin->SetSimulatePhysics(true);
	
	RootComponent = StaticMeshMagazin;
}



// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS/FuncLibrary/Types.h"
#include "GameFramework/Actor.h"
#include "Magazin.generated.h"

UCLASS()
class TPS_API AMagazin : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	//AMagazin(UStaticMesh* MagazineDrop);
	AMagazin();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMeshMagazin = nullptr;

	//UPROPERTY()
	//	FWeaponInfo WeaponSetting;

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "TPSEnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Engine/ActorChannel.h"

// Sets default values
ATPSEnvironmentStructure::ATPSEnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);
}

// Called when the game starts or when spawned
void ATPSEnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPSEnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPSEnvironmentStructure::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;

	UStaticMeshComponent* MyMesh = Cast<UStaticMeshComponent>(UStaticMeshComponent::StaticClass());

	if (MyMesh)
	{
		UMaterialInterface* MyMaterial = MyMesh->GetMaterial(0);
		if (MyMaterial)
		{
			Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}

	return Result;
}

TArray<UTPSStateEffect*> ATPSEnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATPSEnvironmentStructure::RemoveEffect_Implementation(UTPSStateEffect* RemovedEffect)
{
	//Effects.Remove(RemovedEffect);
	Effects.Remove(RemovedEffect);

	if (!RemovedEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemovedEffect, false);
		EffectRemove = RemovedEffect;
	}
}

void ATPSEnvironmentStructure::AddEffect_Implementation(UTPSStateEffect* NewEffect)
{
	//Effects.Add(NewEffect);
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}
}

void ATPSEnvironmentStructure::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void ATPSEnvironmentStructure::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void ATPSEnvironmentStructure::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void ATPSEnvironmentStructure::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, OffsetEffect, NAME_None);
}

void ATPSEnvironmentStructure::SwitchEffect(UTPSStateEffect* Effect, bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			FName NameBoneToAttached = NAME_None;
			FVector Loc = OffsetEffect;

			USceneComponent* mySceneComponent = GetRootComponent();
			if (mySceneComponent)
			{
				UParticleSystemComponent* newParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, mySceneComponent, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(newParticleSystem);
			}
		}
	}
	else
	{
		int32 i = 0;
		bool bIsFind = false;
		if (ParticleSystemEffects.Num() > 0)
		{
			while (i < ParticleSystemEffects.Num() && !bIsFind)
			{
				if (ParticleSystemEffects[i]->Template && Effect->ParticleEffect && Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
				{
					bIsFind = true;
					ParticleSystemEffects[i]->DeactivateSystem();
					ParticleSystemEffects[i]->DestroyComponent();
					ParticleSystemEffects.RemoveAt(i);
				}
				i++;
			}
		}
	}
}

bool ATPSEnvironmentStructure::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

void ATPSEnvironmentStructure::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATPSEnvironmentStructure, Effects);
	DOREPLIFETIME(ATPSEnvironmentStructure, EffectAdd);
	DOREPLIFETIME(ATPSEnvironmentStructure, EffectRemove);

}
